import { v4 as uuid } from "uuid";
import { component, inject } from "tsdi";
import { computed, observable, action } from "mobx";
import { Word } from "../../common";
import { CurrentWord } from "./current-word";

export interface Player {
    id: string;
    guessedWords: Word[];
    name: string;
}

export interface Team {
    id: string;
    name: string;
    players: string[];
}

@component
export class Players {
    @inject private current: CurrentWord;

    @observable public teams = new Map<string, Team>();
    @observable public players = new Map<string, Player>();
    @observable public round = 0;

    @computed public get currentTeam() {
        return this.teamList[this.round % this.teams.size];
    }

    @computed public get currentPlayer() {
        return this.getPlayers(this.currentTeam)[Math.floor(this.round / this.teams.size) % 2];
    }

    @computed public get teamList() {
        return Array.from(this.teams.values());
    }

    @computed public get playerList() {
        return Array.from(this.players.values());
    }

    public getPlayers(team: Team) {
        return team.players.map(id => this.players.get(id));
    }

    public getPlayerScore(player: Player) {
        return player.guessedWords.length;
    }

    public getTeamScore(team: Team) {
        return this.getPlayers(team)
            .map(player => this.getPlayerScore(player))
            .reduce((sum, playerScore) => sum + playerScore, 0);
    }

    @computed public get canStart() {
        return this.teamList.length >= 2 && this.teamList.every(team => team.players.length === 2);
    }

    @action.bound public addTeam(name: string) {
        const id = uuid();
        this.teams.set(id, {
            id,
            name,
            players: [],
        });
    }

    @action.bound public addPlayer(name: string, team: string) {
        const id = uuid();
        this.players.set(id, {
            id,
            name,
            guessedWords: [],
        });
        this.teams.get(team).players.push(id);
    }

    @action.bound public wordGuessed() {
        this.currentPlayer.guessedWords.push(this.current.selectedWord);
    }

    @action.bound public nextRound() {
        this.round++;
    }
}
