import { component, inject } from "tsdi";
import { computed, observable, action } from "mobx";
import { Options } from "./options";
import { Word } from "../../common";

@component
export class CurrentWord {
    @inject private options: Options;

    @observable public words: Word[] = [];
    @observable public selected: number;

    @action.bound public async newWords() {
        this.words = [
            await this.options.getRandomWord(),
            await this.options.getRandomWord(),
            await this.options.getRandomWord(),
        ];
        this.selected = undefined;
    }

    @computed public get selectedWord() {
        if (this.selected === undefined) { return; }
        return this.words[this.selected];
    }

    @action.bound public selectWord(word: Word) {
        this.selected = this.words.indexOf(word);
    }
}
