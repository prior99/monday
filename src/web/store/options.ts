import { component, initialize, inject } from "tsdi";
import { computed, observable, action } from "mobx";
import { WordType, defaultWordTypes, Words } from "../../common";

function toQueryString(obj: { [key: string]: string | number }): string {
    return Object.keys(obj)
        .filter(key => obj[key] !== undefined && obj[key] !== null)
        .map(key => `${key}=${obj[key]}`)
        .join("&");
}

function parseQueryString(str: string): { [key: string]: string | number } {
    return str.replace("?", "")
        .split("&")
        .map(pair => pair.split("="))
        .reduce((result, [key, value]) => ({ ...result, [key]: value }), {});
}

export interface DifficultyConfiguration {
    minFreq?: number;
    maxFreq?: number;
}

export interface Difficulties {
    easy: DifficultyConfiguration;
    medium: DifficultyConfiguration;
    hard: DifficultyConfiguration;
}

export type Difficulty = keyof Difficulties;

export const difficulties: Difficulties = {
    easy: {
        minFreq: 200000,
    },
    medium: {
        minFreq: 50000,
        maxFreq: 200000,
    },
    hard: {
        maxFreq: 50000,
    },
};

@component
export class Options {
    @inject private words: Words;

    @observable public wordTypes = defaultWordTypes;
    @observable public count = 0;
    @observable public totalCount = 0;
    @observable public difficulty: Difficulty = "easy";

    @initialize
    protected async initialize() {
        this.fromUrl();
        await this.refreshCount();
        this.totalCount = await this.words.getWordCount();
    }

    @action.bound private async refreshCount() {
        this.count = await this.words.getWordCount(this.wordTypes.join(","), this.minFreq, this.maxFreq);
    }

    @action.bound public async getRandomWord() {
        return await this.words.getRandomWord(this.wordTypes.join(","), this.minFreq, this.maxFreq);
    }

    private serializeToUrl() {
        const { difficulty, wordTypes } = this;
        const queryString = toQueryString({
            difficulty,
            wordTypes: wordTypes.join(","),
        });
        history.pushState(null, null, `?${queryString}`);
    }

    private fromUrl() {
        const { difficulty, wordTypes } = parseQueryString(location.search);
        this.difficulty = difficulty !== undefined ? String(difficulty) as Difficulty : "easy";
        if (wordTypes !== undefined) {
            if (wordTypes === "") {
                this.wordTypes = [];
            } else {
                this.wordTypes = String(wordTypes).split(",") as WordType[];
            }
        }
    }

    public toggleWordType(wordType: WordType) {
        return action(async () => {
            if (this.isWordTypeActive(wordType)) {
                this.wordTypes.splice(this.wordTypes.indexOf(wordType), 1);
            } else {
                this.wordTypes.push(wordType);
            }
            this.serializeToUrl();
            await this.refreshCount();
        });
    }

    public isWordTypeActive(wordType: WordType) {
        return this.wordTypes.includes(wordType);
    }

    @computed public get minFreq() {
        return difficulties[this.difficulty].minFreq;
    }

    @computed public get maxFreq() {
        return difficulties[this.difficulty].maxFreq;
    }

    public setDifficulty(difficulty: Difficulty) {
        return action(async () => {
            this.difficulty = difficulty;
            await this.refreshCount();
            this.serializeToUrl();
        });
    }
}
