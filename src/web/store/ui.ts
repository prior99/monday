import { component } from "tsdi";
import {  observable, action } from "mobx";

export enum Phase {
    CONFIGURE = "configure",
    PLAYERS = "players",
    STATUS = "status",
    SELECT_WORD = "select word",
    PLAY = "play",
}

@component
export class Ui {
    @observable public phase = Phase.CONFIGURE;

    @action.bound public configurationDone() {
        this.phase = Phase.PLAYERS;
    }

    @action.bound public playersDefined() {
        this.phase = Phase.STATUS;
    }

    @action.bound public startPlay() {
        this.phase = Phase.SELECT_WORD;
    }

    @action.bound public wordSelected() {
        this.phase = Phase.PLAY;
    }

    @action.bound public timeOver() {
        this.phase = Phase.STATUS;
    }

    @action.bound public wordDone() {
        this.phase = Phase.SELECT_WORD;
    }
}
