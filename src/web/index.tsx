import * as React from "react";
import * as ReactDOM from "react-dom";
import DevTools from "mobx-react-devtools";
import { TSDI } from "tsdi";
import { configureController, ControllerOptions } from "hyrest";
import { isProductionEnvironment, allControllers } from "../common";
import { App } from "./components";

declare var baseUrl: string;

const tsdi: TSDI = new TSDI();
tsdi.enableComponentScanner();

const controllerOptions: ControllerOptions = {
    baseUrl,
    errorHandler: err => console.error(err),
};

configureController(allControllers, controllerOptions);

ReactDOM.render(
    <div>
        <App />
        {!isProductionEnvironment() && <DevTools position={{ bottom: 0 }} />}
    </div>,
    document.getElementById("root"),
);
