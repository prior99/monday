import * as React from "react";
import { Players } from "../store";
import { inject, external } from "tsdi";
import { observer } from "mobx-react";
import { observable, computed, action } from "mobx";
import { Player } from "./player";

export interface TeamProps {
    id: string;
    editable?: boolean;
}

@observer @external
export class Team extends React.Component<TeamProps> {
    @inject private players: Players;

    @observable private playerName = "";

    @action.bound private handleAddPlayer(event: React.SyntheticEvent<HTMLFormElement>) {
        event.preventDefault();
        this.players.addPlayer(this.playerName, this.props.id);
        this.playerName = "";
    }

    @action.bound private handlePlayerNameChange(event: React.SyntheticInputEvent) {
        this.playerName = event.target.value;
    }

    @computed private get team() {
        return this.players.teams.get(this.props.id);
    }

    public render() {
        return (
            <div
                style={{
                    border: "1px solid #AAA",
                    padding: 10,
                    borderRadius: 5,
                    margin: 10,
                }}
            >
                <h5 className="title is-5">{this.team.name}</h5>
                <p>Team scored <b>{this.players.getTeamScore(this.team)}</b> points</p>
                <br />
                <h6 className="title is-6">Players</h6>
                {
                    this.players.getPlayers(this.team).map(player => <Player id={player.id} />)
                }
                <br />
                {
                    this.props.editable && (
                        <form onSubmit={this.handleAddPlayer}>
                            <h6 className="title is-6">Add Player</h6>
                            <div className="field">
                                <label className="label">Name</label>
                                <div className="control">
                                    <input
                                        className="input"
                                        type="text"
                                        value={this.playerName}
                                        onChange={this.handlePlayerNameChange}
                                    />
                                </div>
                            </div>
                            <div className="field">
                                <input
                                    type="submit"
                                    className="button"
                                    disabled={this.team.players.length >= 2}
                                    value="Add Player"
                                />
                            </div>
                        </form>
                    )
                }
            </div>
        );
    }
}
