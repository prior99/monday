import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Ui, Phase } from "../store";
import { Configurator } from "./configurator";
import { WordSelection } from "./word-selection";
import { Status } from "./status";
import { Play } from "./play";
import { DefinePlayers } from "./define-players";

@external @observer
export class App extends React.Component<{}, undefined> {
    @inject private ui: Ui;

    private renderContent() {
        switch (this.ui.phase) {
            case Phase.SELECT_WORD:
                return <WordSelection />;
            case Phase.PLAYERS:
                return <DefinePlayers />;
            case Phase.STATUS:
                return <Status />;
            case Phase.PLAY:
                return <Play />;
            default:
            case Phase.CONFIGURE:
                return <Configurator />;
        }
    }

    public render() {
        return (
            <div className="container">
                {this.renderContent()}
            </div>
        );
    }
}
