import * as React from "react";
import { Options, Ui } from "../store";
import { inject, external } from "tsdi";
import { observer } from "mobx-react";

@observer @external
export class Configurator extends React.Component<{}, undefined> {
    @inject private options: Options;
    @inject private ui: Ui;

    public render() {
        return (
            <div>
                <h1 className="title is-4">Konfigurieren</h1>
                <div className="field">
                    <label className="label">Schwierigkeit</label>
                    <div className="control">
                        <p>
                            <label className="checkbox">
                                <input
                                    type="checkbox"
                                    checked={this.options.isWordTypeActive("noun")}
                                    onChange={this.options.toggleWordType("noun")}
                                />
                                Nomen
                            </label>
                        </p>
                        <p>
                            <label className="checkbox">
                                <input
                                    type="checkbox"
                                    checked={this.options.isWordTypeActive("verb")}
                                    onChange={this.options.toggleWordType("verb")}
                                />
                                Verben
                            </label>
                        </p>
                        <p>
                            <label className="checkbox">
                                <input
                                    type="checkbox"
                                    checked={this.options.isWordTypeActive("adjective")}
                                    onChange={this.options.toggleWordType("adjective")}
                                />
                                Adjektive
                            </label>
                        </p>
                    </div>
                </div>
                <div className="field">
                    <label className="label">Schwierigkeit</label>
                    <div className="control">
                        <p>
                            <label className="radio">
                                <input
                                    type="radio"
                                    name="difficulty"
                                    checked={this.options.difficulty === "easy"}
                                    onClick={this.options.setDifficulty("easy")}
                                />
                                Einfach
                            </label>
                        </p>
                        <p>
                            <label className="radio">
                                <input
                                    type="radio"
                                    name="difficulty"
                                    checked={this.options.difficulty === "medium"}
                                    onClick={this.options.setDifficulty("medium")}
                                />
                                Normal
                            </label>
                        </p>
                        <p>
                            <label className="radio">
                                <input
                                    type="radio"
                                    name="difficulty"
                                    checked={this.options.difficulty === "hard"}
                                    onClick={this.options.setDifficulty("hard")}
                                />
                                Hart
                            </label>
                        </p>
                    </div>
                </div>
                <div className="field">
                    <label className="label">Words</label>
                    <label className="control">{this.options.count}</label>
                </div>
                <div className="field">
                    <button className="button is-primary" onClick={this.ui.configurationDone}>Done</button>
                </div>
            </div>
        );
    }
}
