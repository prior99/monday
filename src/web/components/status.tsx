import * as React from "react";
import { Players, Ui } from "../store";
import { inject, external } from "tsdi";
import { observer } from "mobx-react";
import { Team } from "./team";

@observer @external
export class Status extends React.Component<{}, undefined> {
    @inject private players: Players;
    @inject private ui: Ui;

    public render() {
        return (
            <div>
                <h1 className="title is-4">Runde {this.players.round + 1}</h1>
                {
                    this.players.teamList.map(team => <Team id={team.id} />)
                }
                <h1 className="title is-4">Continue</h1>
                <p>
                    Next Player is <b>{this.players.currentPlayer.name}</b>{" "}
                    from Team <b>{this.players.currentTeam.name}</b>.
                </p>
                <br />
                <button className="button is-primary" onClick={this.ui.startPlay}>Select Word</button>
            </div>
        );
    }
}
