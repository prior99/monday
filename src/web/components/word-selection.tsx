import * as React from "react";
import { CurrentWord, Ui } from "../store";
import { inject, external, initialize } from "tsdi";
import { observer } from "mobx-react";
import { action } from "mobx";
import { Word } from "../../common";

@observer @external
export class WordSelection extends React.Component<{}, undefined> {
    @inject private current: CurrentWord;
    @inject private ui: Ui;

    @initialize protected async initialize() {
        await this.current.newWords();
    }

    @action.bound private selectWord(word: Word) {
        this.current.selectWord(word);
        this.ui.wordSelected();
    }

    private renderWord(word: Word) {
        return (
            <p>
                <button
                    className="button"
                    style={{ width: "100%", marginBottom: 10 }}
                    onClick={() => this.selectWord(word)}
                >
                    {word.word}
                </button>
                <br />
            </p>
        );
    }

    public render() {
        return (
            <div>
                {this.current.words.map(word => this.renderWord(word))}
            </div>
        );
    }
}
