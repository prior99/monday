import * as React from "react";
import { Players } from "../store";
import { inject, external } from "tsdi";
import { observer } from "mobx-react";
import { observable, action } from "mobx";

@observer @external
export class AddTeam extends React.Component<{}, undefined> {
    @inject private players: Players;

    @observable private name = "";

    @action.bound private handleAddTeam(event: React.SyntheticEvent<HTMLFormElement>) {
        event.preventDefault();
        this.players.addTeam(this.name);
        this.name = "";
    }

    @action.bound private handleNameChange(event: React.SyntheticInputEvent) {
        this.name = event.target.value;
    }

    public render() {
        return (
            <form onSubmit={this.handleAddTeam}>
                <div className="field">
                    <label className="label">Name</label>
                    <div className="control">
                        <input className="input" type="text" value={this.name} onChange={this.handleNameChange} />
                    </div>
                </div>
                <div className="field">
                    <input className="button" type="submit" value="Add Team" />
                </div>
            </form>
        );
    }
}
