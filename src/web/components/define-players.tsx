import * as React from "react";
import { Players, Ui } from "../store";
import { inject, external } from "tsdi";
import { observer } from "mobx-react";
import { AddTeam } from "./add-team";
import { Team } from "./team";

@observer @external
export class DefinePlayers extends React.Component<{}, undefined> {
    @inject private players: Players;
    @inject private ui: Ui;

    public render() {
        return (
            <div>
                <h1 className="title is-4">Teams</h1>
                {
                    this.players.teamList.map(team => <Team editable id={team.id} />)
                }
                <AddTeam />
                <button
                    className="button is-primary"
                    onClick={this.ui.playersDefined}
                    disabled={!this.players.canStart}
                >
                    Done
                </button>
            </div>
        );
    }
}
