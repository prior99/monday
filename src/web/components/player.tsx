import * as React from "react";
import { Players } from "../store";
import { inject, external } from "tsdi";
import { observer } from "mobx-react";
import { computed } from "mobx";

export interface PlayerProps {
    id: string;
}

@observer @external
export class Player extends React.Component<PlayerProps> {
    @inject private players: Players;

    @computed private get player() {
        return this.players.players.get(this.props.id);
    }

    public render() {
        return (
            <div>
                {this.player.name} ({this.players.getPlayerScore(this.player)})
            </div>
        );
    }
}
