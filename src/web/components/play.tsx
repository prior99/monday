import * as React from "react";
import { inject, external } from "tsdi";
import { observer } from "mobx-react";
import { action, computed } from "mobx";
import { CurrentWord, Ui, Players } from "../store";

@observer @external
export class Play extends React.Component<{}, undefined> {
    @inject private current: CurrentWord;
    @inject private ui: Ui;
    @inject private players: Players;

    @computed private get wordString() {
        if (!this.current.selectedWord) { return ""; }
        return this.current.selectedWord.word;
    }

    @action.bound private done() {
        this.players.wordGuessed();
        this.ui.wordDone();
    }

    @action.bound private timeOver() {
        this.ui.timeOver();
        this.players.nextRound();
    }

    public render() {
        return (
            <div>
                <h1 className="title is-1">{this.wordString}</h1>
                <button className="button is-primary" onClick={this.done}>Done</button>
                <button className="button" onClick={this.timeOver}>Time Over</button>
            </div>
        );
    }
}
