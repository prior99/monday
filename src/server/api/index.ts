import * as Express from "express";
import { Socket } from "net";
import * as BodyParser from "body-parser";
import { Server } from "http";
import { component, inject, initialize, TSDI, destroy } from "tsdi";
import { info } from "winston";
import { bind } from "decko";
import { hyrest } from "hyrest/middleware";
import { AuthorizationMode, configureController, ControllerMode } from "hyrest";
import * as morgan from "morgan";
import { ServerConfig } from "../../config";
import { allControllers } from "../../common";
import { cors, catchError } from "./middlewares";

@component
export class RestApi {
    @inject private config: ServerConfig;
    @inject private tsdi: TSDI;

    private connections = new Set<Socket>();
    public app: Express.Application;
    private server: Server;

    @initialize
    protected initialize() {
        configureController(
            allControllers,
            { mode: ControllerMode.SERVER },
        );
        this.app = Express();
        this.app.use(BodyParser.json());
        this.app.use(BodyParser.urlencoded({ extended: true }));
        this.app.use(morgan("tiny", { stream: { write: msg => info(msg.trim()) } }));
        this.app.use(cors);
        this.app.use(catchError);
        this.app.use(
            hyrest(...allControllers.map((controller: any) => this.tsdi.get(controller)))
                .defaultAuthorizationMode(AuthorizationMode.NOAUTH),
        );
    }

    public serve() {
        const port = this.config.port;
        this.server = this.app.listen(port);
        this.server.on("connection", this.onConnection);
        info(`Api started, listening on port ${port}.`);
    }

    @bind private onConnection(conn: Socket) {
        this.connections.add(conn);
        conn.on("close", () => this.connections.delete(conn));
    }

    @destroy
    public stop() {
        return new Promise((resolve, reject) => {
            if (!this.server) { return; }
            this.server.close(() => {
                info(`Terminating ${this.connections.size} open websocket connections.`);
                for (const socket of this.connections) {
                    socket.destroy();
                    this.connections.delete(socket);
                }
                info("Api stopped.");
                resolve();
            });
        });
    }
}
