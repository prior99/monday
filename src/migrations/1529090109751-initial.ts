import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Initial1529090109751 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Initial");
        await queryRunner.query(`
            CREATE TABLE "word" (
                "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
                "word" text NOT NULL,
                "type" character varying(16) NOT NULL,
                "frequency" double precision NOT NULL
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Initial");
        await queryRunner.query(`DROP TABLE "word"`);
    }
}
