import { CLI, Shim } from "clime";
import { setupWinston } from "./common";

setupWinston();

const cli = new CLI("monday", `${__dirname}/commands`);
const shim = new Shim(cli);
shim.execute(process.argv);
