import { readFileSync } from "fs";
import { metadata, command, Command, param } from "clime";
import { TSDI } from "tsdi";
import { error, info } from "winston";
import { Connection } from "typeorm";
import { ServerConfig, ServerConfigFactory } from "../config";
import { DatabaseFactory } from "../server";
import { Word } from "../common";
import { WordType } from "../common/word-type";

const wordTypeMapping: { [key: string]: WordType } = {
    ADJA: "adjective",
    ADJD: "adjective",
    ADV: "adverb",
    APPO: "apposition",
    APPR: "preposition",
    ART: "article",
    ITJ: "interjection",
    KON: "conjunction",
    KOUS: "conjunction",
    NN: "noun",
    PDAT: "pronoun",
    PDS: "pronoun",
    PIAT: "quantifier",
    PIS: "quantifier",
    PPER: "pronoun",
    PPOSAT: "pronoun",
    PRELS: "pronoun",
    PROAV: "adverb",
    PTKA: "adverb",
    PTKANKT: "particle",
    PTKNEG: "particle",
    PTKVZ: "adverb",
    PWAV: "adverb",
    VVFIN: "verb",
    VVINF: "verb",
    VVIZU: "verb",
    VVPP: "verb",
};

@command({ description: "Import a dictionary. Intended to be used with DeReKo-2014-II-MainArchive-STT.100000 from http://www1.ids-mannheim.de/kl/projekte/methoden/derewo.html" }) // tslint:disable-line
export default class ImportCommand extends Command { // tslint:disable-line

    @metadata
    public async execute(
        @param({ required: true, description: "Dictionary file." }) fileName: string,
        config: ServerConfig,
    ) {
        process.on("unhandledRejection", err => {
            error(`Unhandled Promise rejection: ${err.message}`);
            console.error(err);
        });
        process.on("uncaughtException", err => {
            error(`Unhandled Promise rejection: ${err.message}`);
            console.error(err);
        });
        // Load the configuration.
        if (!config.load()) { return; }
        const tsdi = new TSDI();
        tsdi.enableComponentScanner();
        // Initialize config.
        tsdi.get(ServerConfigFactory).setConfig(config);
        // Initialize database.
        await tsdi.get(DatabaseFactory).connect();
        const db = await tsdi.get(Connection);

        info("Opening file ...");
        const content = readFileSync(fileName, "utf8");
        info("Splitting lines ...");
        const lines = content.split("\n");
        info(`Importing ${lines.length} lines ...`);
        let importedCount = 0;
        for (let lineCount = 0; lineCount < lines.length; ++lineCount) {
            const line = lines[lineCount];
            const columns = line.split(/\s/);
            if (columns.length !== 4) {
                error(`Cannot import line ${lineCount}: "${line}" (Invalid field count)`);
                continue;
            }
            const [ _, word, originalType, frequencyString ] = columns; // tslint:disable-line
            if (word.includes(".")) { continue; }
            let frequency: number;
            try {
                frequency = Number(frequencyString);
            } catch (err) {
                error(`Cannot import line ${line}: "${line}" (Invalid number format)`);
                continue;
            }
            const type = wordTypeMapping[originalType];
            if (!type) { continue; }
            const existing = Boolean(await db.getRepository(Word).findOne({ word }));
            if (existing) { continue; }
            await db.getRepository(Word).save({ word, type, frequency });
            info(`Imported word "${word}" with type ${type} and frequency ${frequency.toFixed(0)}`);
            importedCount++;
        }
        info(`Done. ${importedCount} new words imported.`);
        tsdi.close();
    }
}
