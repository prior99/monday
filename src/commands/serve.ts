import { metadata, command, Command } from "clime";
import { TSDI } from "tsdi";
import { error, warn } from "winston";
import { ServerConfig, ServerConfigFactory } from "../config";
import { RestApi, DatabaseFactory } from "../server";

@command({ description: "Start the API." })
export default class ServeCommand extends Command { // tslint:disable-line
    @metadata
    public async execute(config: ServerConfig) {
        process.on("unhandledRejection", err => {
            error(`Unhandled Promise rejection: ${err.message}`);
            console.error(err);
        });
        process.on("uncaughtException", err => {
            error(`Unhandled Promise rejection: ${err.message}`);
            console.error(err);
        });
        // Load the configuration.
        if (!config.load()) { return; }
        const tsdi = new TSDI();
        tsdi.enableComponentScanner();
        // Initialize config.
        tsdi.get(ServerConfigFactory).setConfig(config);
        // Initialize database.
        await tsdi.get(DatabaseFactory).connect();
        // Start api.
        tsdi.get(RestApi).serve();

        let killed = false;
        const kill = async () => {
            if (killed) {
                error("CTRL^C detected. Terminating!");
                process.exit(1);
                return;
            }
            killed = true;
            warn("CTRL^C detected. Secure shutdown initiated.");
            warn("Press CTRL^C again to terminate at your own risk.");
            tsdi.close();
        };
        process.on("SIGINT", kill);
    }
}
