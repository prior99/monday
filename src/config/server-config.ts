import { option, Options } from "clime";
import { error } from "winston";
import { pickBy } from "ramda";

import { loadConfigFile } from "./load-config-file";

export class ServerConfig extends Options {
    @option({ flag: "p", description: "Port to host the API on." })
    public port: number;

    @option({ flag: "c", description: "Path to the configuration file." })
    public configFile: string;

    @option({ description: "Name of the database to use." })
    public dbName: string;

    @option({ description: "Username to use to connect to the database" })
    public dbUsername: string;

    @option({ description: "Password to use to connect to the database." })
    public dbPassword: string;

    @option({ description: "Port the database runs on." })
    public dbPort: number;

    @option({ description: "Hostname of the server hosting the database." })
    public dbHost: string;

    @option({ description: "Drive of the database. Defaults to \"postgres\"" })
    public dbDriver: string;

    @option({ description: "Whether to log all SQL queries executed." })
    public dbLogging: boolean;

    @option({ description: "Whether SSL should be used to connect to the database." })
    public dbSSL: boolean;

    public load() {
        Object.assign(
            this,
            {
                port: 23079,
                dbPort: 5432,
                dbDriver: "postgres",
                dbLogging: false,
            },
            pickBy(val => val !== undefined, loadConfigFile(this.configFile)),
            pickBy(val => val !== undefined, this),
        );

        let okay = true;
        const check = (value: string | number | boolean, message: string) => {
            if (value === undefined) {
                error(message);
                okay = false;
            }
        };
        check(this.dbName, "Database name not configured. Add 'dbName' to config file or specify --db-name.");
        return okay;
    }
}
