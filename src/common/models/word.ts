import { Column, PrimaryGeneratedColumn, Entity } from "typeorm";
import { is, scope, uuid, oneOf } from "hyrest";
import { world } from "../scopes";
import { WordType, wordTypes } from "../word-type";

@Entity()
export class Word {
    @PrimaryGeneratedColumn("uuid")
    @scope(world) @is().validate(uuid)
    public id?: string;

    @Column("text")
    @is() @scope(world)
    public word?: string;

    @Column("varchar", { length: 16 })
    @is().validate(oneOf(wordTypes)) @scope(world)
    public type?: WordType;

    @Column("double precision")
    @is() @scope(world)
    public frequency?: number;
}
