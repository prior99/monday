import { createScope } from "hyrest";

// General visibility.
export const world = createScope();
