export type WordType =
    "adjective" |
    "apposition" |
    "preposition" |
    "article" |
    "interjection" |
    "conjunction" |
    "noun" |
    "quantifier" |
    "pronoun" |
    "adverb" |
    "particle" |
    "verb";

export const wordTypes: WordType[] = [
    "adjective",
    "apposition",
    "preposition",
    "article",
    "interjection",
    "conjunction",
    "noun",
    "quantifier",
    "pronoun",
    "adverb",
    "particle",
    "verb",
];

export const allowedWordTypes: WordType[] = [
    "adjective",
    "noun",
    "verb",
];

export const defaultWordTypes: WordType[] = [
    "noun",
];
