import { controller, route, ok, query, is } from "hyrest";
import { component, inject } from "tsdi";
import { Connection } from "typeorm";
import { Word } from "../models";
import { world } from "../scopes";

@controller @component
export class Words {
    @inject private db: Connection;

    @route("GET", "/random").dump(Word, world)
    public async getRandomWord(
        @query("wordTypes") @is() types?: string,
        @query("minFreq") @is() minFreq?: number,
        @query("maxFreq") @is() maxFreq?: number,
    ): Promise<Word> {
        const queryBuilder = this.db.getRepository(Word).createQueryBuilder("word")
            .orderBy("random()")
            .limit(1);
        if (types) { queryBuilder.andWhere("type = ANY(:types)", { types: types.split(",") }); }
        if (minFreq) { queryBuilder.andWhere("frequency >= :minFreq", { minFreq }); }
        if (maxFreq) { queryBuilder.andWhere("frequency <= :maxFreq", { maxFreq }); }
        const word = await queryBuilder.getOne();
        return ok(word);
    }

    @route("GET", "/count")
    public async getWordCount(
        @query("wordTypes") @is() types?: string,
        @query("minFreq") @is() minFreq?: number,
        @query("maxFreq") @is() maxFreq?: number,
    ): Promise<number> {
        const queryBuilder = this.db.getRepository(Word).createQueryBuilder("word");
        if (types) { queryBuilder.andWhere("type = ANY(:types)", { types: types.split(",") }); }
        if (minFreq) { queryBuilder.andWhere("frequency >= :minFreq", { minFreq }); }
        if (maxFreq) { queryBuilder.andWhere("frequency <= :maxFreq", { maxFreq }); }
        const count = await queryBuilder.getCount();
        return ok(count);
    }

    @route("GET", "/max-frequency")
    public async getMaxFrequency(): Promise<number> {
        const mostFrequentWord = await this.db.getRepository(Word).createQueryBuilder("word")
            .orderBy({ frequency: "DESC" })
            .getOne();
        return ok(mostFrequentWord.frequency);
    }
}
