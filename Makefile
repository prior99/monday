SHELL:=/bin/bash

default: debug

all: default lint db

.PHONY: build-server
build-server:
	yarn build:server

.PHONY: debug
debug: node_modules build-server
	yarn build:web

.PHONY: release
release: node_modules build-server
	yarn build:web:release

.PHONY: node_modules
node_modules:
	yarn

.PHONY: lint
lint: node_modules
	yarn lint

.PHONY: run-web
run-web: node_modules
	yarn start:web

.PHONY: run-server
run-server: node_modules db
	yarn start:server
		--db-password $${POSTGRES_PASSWORD:-""}

.PHONY: clean-db
clean-db:
	dropdb monday || true

.PHONY: clean
clean:
	rm -Rf node_modules/
	rm -Rf server/
	rm -f dist/bundle.js \
	  dist/bundle.js.map \
	  dist/bundle.css \
	  dist/bundle.css.map || true

.PHONY: db
db:
	createdb monday || true
