const Webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const GitRevisionPlugin = require("git-revision-webpack-plugin");
const path = require('path');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');

const gitRevision = new GitRevisionPlugin({ lightweightTags: true });

module.exports = {
    mode: "development",
    entry: {
        "bundle": path.join(__dirname, "src", "web"),
    },
    context: path.join(__dirname, "dist"),
    output: {
        path: path.join(__dirname, "dist"),
        filename: "[name].js",
        publicPath: "/"
    },
    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx", ".json"],
        alias: {
            "clime": path.join(__dirname, "src", "clime-shim.ts"),
        },
    },
    module: {
        rules: [
            {
                test: /\.(png|svg|woff|ttf|woff2|eot)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            hash: "sha512",
                            digest: "hex",
                            name: "[hash].[ext]"
                        }
                    }
                ]
            },
            {
                test: /\.tsx?/,
                loader: "ts-loader",
                exclude: [
                    /__tests__/,
                ],
                options: {
                    configFile: "tsconfig-webpack.json",
                },
            }
        ]
    },
    externals: {
        "child_process": "{}",
        "express": "{}",
        "fs": "{}",
        "morgan": "{}",
        "net": "{}",
        "readline": "{}",
        "winston": "{}",
    },
    devtool: "source-map",
    devServer: {
        port: 3021,
        historyApiFallback: true,
        contentBase: path.join(__dirname, "dist")
    },
    plugins: [
        new Webpack.NormalModuleReplacementPlugin(/typeorm$/, result => {
            result.request = result.request.replace(/typeorm/, "typeorm/browser");
        }),
        new Webpack.DefinePlugin({
            // Taken and adapted from the official README.
            // See: https://www.npmjs.com/package/git-revision-webpack-plugin
            "SOFTWARE_VERSION": JSON.stringify(gitRevision.version())
        }),
        new ProgressBarPlugin()
    ],
};
